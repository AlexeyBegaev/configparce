#ifndef CONFIGPARSER_UTILS_H
#define CONFIGPARSER_UTILS_H

#include <string>

namespace uts
{
/*!
 * @brief Получение параметра param конфигурационного файла cfgFile(Darknet format)
 * @date 23.07.2020
 * @author BegaevAlexey
 * @param cfgFile[in] Конфигурационный файл в формате yolo.cfg(Darknet)
 * @param param[in] Н
 */
    float getParam(const std::string &cfgFile, const std::string &param);
} // namespace uts

#endif //CONFIGPARSER_UTILS_H
