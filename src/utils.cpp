#include "utils.h"

#include <fstream>
#include <iostream>

float uts::getParam(const std::string &cfgFile, const std::string &param)
{
    std::ifstream file(cfgFile);
    if(!file.is_open())
    {
        std::cerr << "[Warning] Config file " << cfgFile << " was not opened!\n";
        return -1;
    }

    if(param.empty())
    {
        std::cerr << "[Warning] Name of param is empty!\n";
        return -1.f;
    }

    std::string line;
    while (std::getline(file, line))
    {
        std::size_t found = line.find(param);
        if ((found != std::string::npos) && !(found))
        {
            found = line.find("=");
            return std::atof(line.substr(found+1).c_str());
        }

    }

    return -1.f;
}
