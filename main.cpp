#include <iostream>

#include "src/utils.h"

int main(int argc, char** argv)
{
    if(argc < 2)
    {
        std::cerr << "[ERROR] Not enough arguments" << std::endl;
        exit(EXIT_FAILURE);
    }

    int channels = uts::getParam(argv[1], "channels");
    std::cout << "[INFO] Count of channels: " << channels << std::endl;

    std::cout << "\n\t***END PROGRAM***" << std::endl;
    return 0;
}
